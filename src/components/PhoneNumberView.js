import React, { useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import Colors from '../Utils/Colors';
import firebase from 'react-native-firebase';
import { GoogleSignin } from 'react-native-google-signin';

const width = Dimensions.get('window').width;

export default function PhoneNumberView() {
  const [phoneNumber, setPhoneNumber] = useState('');
  function checkDisabled() {
    if (phoneNumber === '' || phoneNumber.length <= 9) {
      return true;
    } else if (phoneNumber.length === 10) {
      return false;
    }
  }
   function handleSubmit() {
    
  }
  return (
    <View style={styles.container}>
      <View styles={{ flex: 1 }}>
        <Text style={styles.notifyText}> Get notified, </Text>
        <Text style={styles.alarmText}> never miss a dose </Text>
        <TextInput
          placeholder="Phone Number"
          placeholderTextColor={Colors.placeholderGrey}
          style={styles.textInput}
          maxLength={10}
          keyboardType="numeric"
          onChangeText={number => setPhoneNumber(number)}
          value={phoneNumber}
        />
        <TouchableOpacity
          onPress={handleSubmit}
          disabled={checkDisabled()}
          style={checkDisabled() ? styles.disabledButton : styles.button}
        >
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={handleSubmit}
          style={ styles.button}
        >
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  alarmText: {
    fontSize: 27,
    letterSpacing: 0.3,
    fontWeight: '200'
  },
  textInput: {
    height: 50,
    width: width * 0.85,
    borderColor: Colors.darkGrey,
    borderWidth: 1,
    borderRadius: 7,
    paddingHorizontal: 10,
    marginTop: 35,
    marginBottom: 30
  },
  disabledButton: {
    width: width * 0.85,
    height: 60,
    backgroundColor: Colors.primaryFaded,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7
  },
  notifyText: {
    fontSize: 32
  },
  button: {
    width: width * 0.85,
    height: 60,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7
  },
  buttonText: {
    color: Colors.white,
    fontWeight: '700'
  }
});
