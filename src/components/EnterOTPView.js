import React, { useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import Colors from '../Utils/Colors';
import OtpInputs from 'react-native-otp-inputs';

const width = Dimensions.get('window').width;

export default function PhoneNumberView() {
  const [timer, setTimer] = useState(30);
  const [OTP, setOTP] = useState('');
  handleResend = () => {
    setTimer(30);
  };
  function checkDisabled() {
    if (OTP === '' || OTP.length <= 3) {
      return true;
    } else if (OTP.length === 4) {
      return false;
    }
  }
  return (
    <View style={styles.container}>
      <View
        styles={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
      >
        <Text style={styles.notifyText}> Enter OTP </Text>
        <OtpInputs
          handleChange={OTP => setOTP(OTP)}
          numberOfInputs={4}
          inputStyles={styles.inputStyles}
          inputContainerStyles={styles.inputContainerStyles}
          containerStyles={styles.inputsContainerStyles}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginBottom: 18
          }}
        >
          <Text style={{ color: Colors.placeholderGrey, marginRight: 10 }}>
            00:{timer}
          </Text>
          <TouchableOpacity onPress={() => handleResend()}>
            <Text
              style={{
                color: Colors.placeholderGrey,
                textDecorationLine: 'underline'
              }}
            >
              Resend OTP
            </Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          disabled={checkDisabled()}
          style={checkDisabled() ? styles.disabledButton : styles.button}
        >
          <Text style={styles.buttonText}>Confirm OTP</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  inputContainerStyles: {
    borderColor: Colors.darkGrey,
    borderWidth: 1,
    borderRadius: 7,
    width: 50,
    height: 50,
    alignItems: 'center'
  },
  inputsContainerStyles: {
    maxHeight: 60,
    marginTop: 25,
    marginBottom: 10
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  alarmText: {
    fontSize: 27,
    letterSpacing: 0.3,
    fontWeight: '200'
  },
  textInput: {
    height: 50,
    width: width * 0.85,
    borderColor: Colors.darkGrey,
    borderWidth: 1,
    borderRadius: 7,
    paddingHorizontal: 10,
    marginTop: 25,
    marginBottom: 30
  },
  notifyText: {
    fontSize: 32
  },
  button: {
    width: width * 0.85,
    height: 60,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7
  },
  disabledButton: {
    width: width * 0.85,
    height: 60,
    backgroundColor: Colors.primaryFaded,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7
  },
  buttonText: {
    color: Colors.white,
    fontWeight: '700'
  }
});
