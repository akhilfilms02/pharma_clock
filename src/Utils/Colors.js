const Colors = {
  primary: '#E71561',
  faintGrey: '#F8F8F8',
  darkGrey: '#D2D2D2',
  black: '#333',
  white: '#fff',
  placeholderGrey: '#888',
  primaryFaded: '#F476A4'
};
export default Colors;
